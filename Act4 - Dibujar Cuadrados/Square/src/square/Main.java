package square;

import java.util.Stack;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

public class Main extends Application {

	int cont = 0;
	int tam = 0;
	double clickUnoX = 0;
	double clickUnoY = 0;
	double incrementoX = 0;
	double incrementoY = 0;
	int aux = 0;
	Circle circle;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Group root = new Group();
		Scene scene = new Scene(root, 800, 600, Color.rgb(238, 238, 238));
		primaryStage.setTitle("Lienzo de cuadrados");
		primaryStage.setScene(scene);
		primaryStage.show();

		Stack<Double> x0 = new Stack<>();
		Stack<Double> y0 = new Stack<>();

		scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent click) {
				circle = new Circle();
				x0.push(click.getX());
				y0.push(click.getY());

				if (x0.size() == 2) {
					double x1 = (double) x0.peek();
					double y1 = (double) y0.peek();
					x0.pop();
					y0.pop();
					double x = (double) x0.peek();
					double y = (double) y0.peek();
					x0.pop();
					y0.pop();

					tam = (int) Math.abs(x1 - x);
					aux = (int) Math.abs(y1 - y);
					if (aux > tam) {
						tam = aux;
					}
					double auxX = x;
					incrementoX = ((x1 - x) / tam);
					auxX++;
					for (int i = 1; i <= tam; ++i) {
						circle = new Circle();
						auxX = auxX + incrementoX;
						circle.setCenterX(auxX);
						circle.setCenterY(y);
						circle.setRadius(2.0f);
						root.getChildren().addAll(circle);
					}

					tam = (int) Math.abs(x1 - x);
					aux = (int) Math.abs(y1 - y);
					if (aux > tam) {
						tam = aux;
					}
					double auxY = y;
					incrementoY = ((y1 - y) / tam);
					auxY++;
					for (int i = 1; i <= tam; ++i) {
						circle = new Circle();
						auxY = auxY + incrementoY;
						circle.setCenterX(x);
						circle.setCenterY(auxY);
						circle.setRadius(2.0f);
						root.getChildren().addAll(circle);
					}

					if (y <= y1) {
						for (int i = (int) y; i <= y1; i++) {
							circle = new Circle();
							circle.setCenterX(x1);
							circle.setCenterY(i);
							circle.setRadius(2.0f);
							root.getChildren().addAll(circle);
						}
					} else {
						for (int i = (int) y; i >= y1; i--) {
							circle = new Circle();
							circle.setCenterX(x1);
							circle.setCenterY(i);
							circle.setRadius(2.0f);
							root.getChildren().addAll(circle);
						}
					}

					if (x <= x1) {
						for (int i = (int) x; i <= x1; i++) {
							circle = new Circle();
							circle.setCenterX(i);
							circle.setCenterY(y1);
							circle.setRadius(2.0f);
							root.getChildren().addAll(circle);
						}
					} else {
						for (int i = (int) x; i >= x1; i--) {
							circle = new Circle();
							circle.setCenterX(i);
							circle.setCenterY(y1);
							circle.setRadius(2.0f);
							root.getChildren().addAll(circle);
						}
					}
				}
			}
		});
	}
}