num = int(input('Introduzca un numero impar: '))

while num % 2 == 0:
    print('Error, intente nuevamente')
    num = int(input('Introduzca un numero impar: '))

colum = num // 2 + 1

for i in range(colum + 1):
    for j in range(colum - i):
        print(' ', end='')

    for k in range((2 * i) - 1):
        print('*', end='')
    print('')

colum = colum - 1

for i in range(1, colum + 1):
    for j in range(i):
        print(' ',  end='')

    for k in range((colum - i) * 2 + 1):
        print('*',  end='')
    print('')

print('------------------------------------------------')

print('')

for i in range(colum + 1):
    for j in range(colum - i):
        print(' ', end='')

    for k in range((2 * i) + 1):
        if k == 0 or k == 2 * i or i == 0:
            print('*', end='')
        else:
            print(' ', end='')
    print('')

for i in range(colum - 1, -1, -1):
    for j in range(colum - i):
        print(' ',  end='')

    for k in range((i * 2) + 1):
        if k == 0 or i == 0 or k == 2 * i:
            print('*', end='')
        else:
            print(' ', end='')
    print('')
