package lines;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.util.Stack;

public class Main extends Application {

	int cont = 0;
	int tam = 0;
	double clickUnoX = 0;
	double clickUnoY = 0;
	double incrementoX = 0;
	double incrementoY = 0;
	Circle circle;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		Group root = new Group();
		Scene scene = new Scene(root, 800, 600, Color.rgb(238, 238, 238));
		primaryStage.setTitle("Lienzo de lineas");
		primaryStage.setScene(scene);
		primaryStage.show();

		Stack<Double> x0 = new Stack<>();
		Stack<Double> y0 = new Stack<>();

		scene.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent click) {
				circle = new Circle();
				x0.push(click.getX());
				y0.push(click.getY());

				if (x0.size() == 2) {
					double x1 = (double) x0.peek();
					double y1 = (double) y0.peek();
					x0.pop();
					y0.pop();
					double x = (double) x0.peek();
					double y = (double) y0.peek();
					x0.pop();
					y0.pop();
					x0.push(x1);
					y0.push(y1);

					tam = (int) Math.abs(x1 - x);
					if (Math.abs(y1 - y) > tam) {
						tam = (int) Math.abs(y1 - y);
					}
					incrementoX = (double) (x1 - x) / (double) tam;
					incrementoY = (double) (y1 - y) / (double) tam;

					x = x + 1;
					y = y + 1;

					for (int i = 1; i <= tam; ++i) {
						circle = new Circle();
						x = x + incrementoX;
						y = y + incrementoY;
						circle.setCenterX(x);
						circle.setCenterY(y);
						circle.setRadius(2.0f);
						root.getChildren().addAll(circle);
					}
				}
			}
		});
	}
}
